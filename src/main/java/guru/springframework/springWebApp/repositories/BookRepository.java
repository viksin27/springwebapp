package guru.springframework.springWebApp.repositories;

import guru.springframework.springWebApp.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {
}
