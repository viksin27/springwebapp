package guru.springframework.springWebApp.repositories;

import org.springframework.data.repository.CrudRepository;
import guru.springframework.springWebApp.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {
}
