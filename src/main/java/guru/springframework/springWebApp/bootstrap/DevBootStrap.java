package guru.springframework.springWebApp.bootstrap;

import guru.springframework.springWebApp.model.Publisher;
import guru.springframework.springWebApp.repositories.AuthorRepository;
import guru.springframework.springWebApp.repositories.BookRepository;
import guru.springframework.springWebApp.repositories.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import guru.springframework.springWebApp.model.Author;
import guru.springframework.springWebApp.model.Book;
import org.springframework.stereotype.Component;

import javax.transaction.Transaction;

@Component
public class DevBootStrap implements ApplicationListener<ContextRefreshedEvent> {
    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootStrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        Publisher publisherA = new Publisher();
        publisherA.setName("Foo");
        publisherA.setAddress("New York");
        publisherRepository.save(publisherA);

        Publisher publisherB = new Publisher();
        publisherB.setName("Foo A");
        publisherB.setAddress("California");
        publisherRepository.save(publisherB);

        //Eric
        Author eric = new Author("Eric", "Evans");
        Book ddd = new Book("Domain Driven Design", "1234", publisherA);
        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);
        authorRepository.save(eric);
        bookRepository.save(ddd);

        //Rod
        Author rod = new Author("Rod", "Johnson");
        Book noEJB = new Book("J2EE Development without EJB", "23444", publisherB);
        rod.getBooks().add(noEJB);
        authorRepository.save(rod);
        bookRepository.save(noEJB);

       /* authorRepository.deleteAll();
        bookRepository.deleteAll();
        publisherRepository.deleteAll();*/

        //       authorRepository.deleteById(3L);
    }
}
